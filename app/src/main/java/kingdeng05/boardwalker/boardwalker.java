package kingdeng05.boardwalker;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.Stack;
import android.graphics.Matrix;
/**
 * Created by KingD on 19/4/2017.
 */
public class boardwalker extends Activity {
    private static int maxN = 8;
    private ImageView[][] board = new ImageView[maxN][maxN];
    private Context context;
    private Drawable[] drawCells = new Drawable[3];
    private int init_i;
    private int init_j;
    private int init_face;
    private int cur_i;
    private int cur_j;
    private int cur_face;
    private Spinner init_i_spinner;
    private Spinner init_j_spinner;
    private Spinner init_face_spinner;
    private TextView cur_i_textview;
    private TextView cur_j_textview;;
    private TextView cur_face_textview;;
    private Button start_button;
    private Button move_button;
    private Button left_button;
    private Button right_button;
    private static String[] numberOptions = new String[8];
    private static String[] faceOptions = new String[]{"E","S","W","N"};

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.board);
        context = this;
        loadResource();
        boardSetup();
        widgetSetup();
    }

    private void loadResource(){
        drawCells[0] = null;
        drawCells[1] = context.getResources().getDrawable(R.drawable.trump);
        drawCells[2] = context.getResources().getDrawable(R.drawable.cell);
    }

    @SuppressLint("NewApi")
    private void boardSetup() {
        Stack<LinearLayout> myStack = new Stack<LinearLayout>();
        int sizeOfCell = (int) Math.round(getScreenSize()/maxN*0.85);
        LinearLayout.LayoutParams bRow = new LinearLayout.LayoutParams(sizeOfCell*maxN,sizeOfCell);
        LinearLayout.LayoutParams bCell = new LinearLayout.LayoutParams(sizeOfCell,sizeOfCell);
        LinearLayout bLayout = (LinearLayout)findViewById(R.id.checkerboard);
        for(int i = 0; i < maxN; ++i){
            LinearLayout linRow = new LinearLayout(context);
            for(int j = 0; j < maxN; ++j){
                board[i][j] = new ImageView(context);
                board[i][j].setBackground(drawCells[2]);
                linRow.addView(board[i][j], bCell);
            }
            myStack.push(linRow);
        }
        while(!myStack.empty()) {
            bLayout.addView(myStack.pop(), bRow);
        }
    }

    private void widgetSetup(){

        // set up the spinner/dropdown list
        init_i_spinner = (Spinner)findViewById(R.id.init_i);
        init_j_spinner = (Spinner)findViewById(R.id.init_j);
        init_face_spinner = (Spinner) findViewById(R.id.init_face);
        for(int i = 0; i < 8; ++i){
            numberOptions[i] = Integer.toString(i+1);
        }
        ArrayAdapter<String> numberAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, numberOptions);
        ArrayAdapter<String> faceAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, faceOptions);
        init_i_spinner.setAdapter(numberAdapter);
        init_j_spinner.setAdapter(numberAdapter);
        init_face_spinner.setAdapter(faceAdapter);
        init_i_spinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                init_i = position+1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                init_i = 1;
            }
        });
        init_j_spinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                init_j = position+1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                init_j = 1;
            }
        });
        init_face_spinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                init_face = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                init_face = 3;
            }
        });

        // set up the cur-positon-and-orientation textview
        cur_i = 0;
        cur_j = 0;
        cur_face = -1;
        cur_i_textview = (TextView)findViewById(R.id.cur_i);
        cur_j_textview = (TextView)findViewById(R.id.cur_j);
        cur_face_textview = (TextView)findViewById(R.id.cur_face);
        cur_i_textview.setText(Integer.toString(cur_i));
        cur_j_textview.setText(Integer.toString(cur_j));
        cur_face_textview.setText("Not set");

        // set up the start button
        start_button = (Button) findViewById(R.id.start_button);
        move_button = (Button)findViewById(R.id.move);
        left_button = (Button) findViewById(R.id.turnleft);
        right_button = (Button) findViewById(R.id.turnright);
        start_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                updateInfo(init_i,init_j,init_face);
            }
        });
        move_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                moveForward();
            }
        });
        left_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                turnLeft();
            }
        });
        right_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                turnRight();
            }
        });
    }

    private void moveForward(){
        if(cur_face == 3 && cur_j<8)  // move north
            updateInfo(cur_i, cur_j+1, cur_face);
        else if(cur_face == 0 && cur_i<8)  // move east
            updateInfo(cur_i+1, cur_j, cur_face);
        else if(cur_face == 1 && cur_j>1)  // move south
            updateInfo(cur_i, cur_j-1, cur_face);
        else if(cur_face == 2 && cur_i>1)   // move west
            updateInfo(cur_i-1, cur_j, cur_face);
        else if(cur_face == -1){
            Context context = getApplicationContext();
            CharSequence text = "Please specify the initial position and orientation";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
        else{
            Context context = getApplicationContext();
            CharSequence text = "Can't move forward";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }

    private void turnLeft(){
        if(cur_face == -1){
            Context context = getApplicationContext();
            CharSequence text = "Please specify the initial position and orientation";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            return;
        }
        if(cur_face == 0) updateInfo(cur_i,cur_j,3);
        else updateInfo(cur_i, cur_j, cur_face-1);
    }

    private void turnRight(){
        if(cur_face == -1){
            Context context = getApplicationContext();
            CharSequence text = "Please specify the initial position and orientation";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            return;
        }
        if(cur_face == 3) updateInfo(cur_i,cur_j,0);
        else updateInfo(cur_i, cur_j, cur_face+1);
    }

    @SuppressLint("NewApi")
    private void updateInfo(int i, int j, int face){
        if(cur_i > 0 && cur_j > 0){
            board[cur_j - 1][cur_i - 1].setBackground(drawCells[2]);  // clear the previous position
            board[cur_j-1][cur_i - 1].setRotation(0);
        }
        board[j-1][i-1].setBackground(drawCells[1]);    // fill the current position
        System.out.println("The face is" + board[j - 1][i - 1].getRotation() + face*90);
        board[j - 1][i - 1].setRotation(board[j - 1][i - 1].getRotation() + face*90);
        cur_i_textview.setText(Integer.toString(i));
        cur_j_textview.setText(Integer.toString(j));
        cur_face_textview.setText(faceOptions[face]);
        setCurInfo(i, j, face);
    }

    private void setCurInfo(int i, int j, int face){
        cur_i = i;
        cur_j = j;
        cur_face = face;
    }

    private float getScreenSize() {
        Resources resources = context.getResources();
        DisplayMetrics DM = resources.getDisplayMetrics();
        return DM.widthPixels;
    }

}
