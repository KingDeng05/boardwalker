# THIS IS THE README FOR AN 8*8 BOARD PROGRAMMING PROJECT
# AUTHOR: FUHENG DENG
# CONTACT: DFUHENG@ENG.UCSD.EDU

# GENERAL DESCRIPTION
This project includes two parts - BoardWalker App & FindAllPaths Program/algorithm

1. BoardWalker App
This is an android app featured an 8*8 board as the background, 
based on which the users can choose move forward, turn left, turn right on the board,
and view the current position and direction (N, E, S, W)
For example, given the initial input as locations [2,3] and direction N, and the action [M M M M L M R R R] (M - move, L - left, R - right)
the actioned result is location [1 6] direction S

2. FindAllPaths Algorithm
This is an algorithm based on the same setting as above, but the goal is to find all the path combinations given start position&direction,
goal position&direction and maximum actions that can be taken.
For example, given the initial location [2,3], direction N, and goal location [3,4], direction S, maximum steps are 4. The result is [M R M R]

# HOW TO

1. BoardWalker App
1) Open up the project source folder in android studio or eclipse, turn on the emulator.
2) Run the app on the emulator, and then main menu window is poped up, click "start" to start the boardwalker test, click "quit" to quit the app.
3) In the boardwalker page, a pure white 8*8 board is shown initially, since no action is taken
4) Specify the initial location and orientation in the right down corner, 0<i,j<9, face = {'E','S','W','N'}. By default, i = 1, j = 1, face = 'E'.
5) After specifying the initial settings, click start to start the boardwalker test, notice the left down corner, current position and orientation is shown.
6) Click "move" to move forward, "left" to turn left, "right" to turn right, check the resulted change in the current information
7) You may start from 4) over and over again and test the app

2. FindAllPaths algorithm
1) The algorithm is in the subfolder FindAllPaths. Import the program into any C++ IDE you have or an any text editor for independent compiling later
2) Write your own main function to text the program
3) Input for the algorithm is: start location i_start, j_start, direction k_start (char) = {'E','S','W','N'}, 
                               goal location i_goal, j_goal, k_goal (char), 
                               max actions
4) Output of the algorithm is a vector (combination) of vector (paths)
5) Print out all the paths and verify the results