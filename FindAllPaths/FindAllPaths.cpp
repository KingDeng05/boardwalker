/*
	THIS FUNCTION IS TO FIND ALL THE PATHS IN AN 8*8 BOARD, GIVEN A START POSITION/ORIENTATION AND A GOAL POSITION/DIRECTOIN
	AUTHOR: FUHENG DENG
	CONTACT: dfuheng@eng.ucsd.edu
*/


#include <iostream>
#include <vector>

using namespace std;

/*
	algorithm: modified depth-first search
	input: start positions [i_start, j_start], start direction k_start, goal position [i_goal, j_goal], goal direction k_goal, maximum step max_step;
	output: a vector of paths
	idea: there are three options for the robot to choose, either move forward, or turn left, or turn right. Hence, in each recursive call, attempt 
	these three options, during which recursive call, check if the position and direction are satisfied or not, if yes, add to result, if not, keep
	searching. If maximum step size is reached, return the function call. When attempting to move, check the boundary if it can move or not.
	Run time: each move has three options, so the total time would be 3^max_step, which is O(2^N)
*/

/*
	Helper function to recursively find all the paths
*/
void explore(vector<vector<char>>& res, int i_start, int j_start, int k_start, int i_goal, int j_goal, int k_goal, vector<char> path, int max_step) {
	if (i_start == i_goal && j_start == j_goal && k_start == k_goal) res.push_back(path);  // if a path is found, add it to the result
	if (!max_step) return;  // if reaches max step sizes
	if (k_start == 0 && i_start < 8) {   // move towards east
		path.push_back('M');
		explore(res, i_start + 1, j_start, k_start, i_goal, j_goal, k_goal, path, max_step - 1);
		path.pop_back();
	}
	else if (k_start == 1 && j_start > 1) {  // move towards south
		path.push_back('M');
		explore(res, i_start, j_start-1, k_start, i_goal, j_goal, k_goal, path, max_step - 1);
		path.pop_back();
	}
	else if (k_start == 2 && i_start > 1) {  // move towards west
		path.push_back('M');
		explore(res, i_start-1, j_start, k_start, i_goal, j_goal, k_goal, path, max_step - 1);
		path.pop_back();
	}
	else if(k_start == 3 && j_start < 8) {  // move towards north
		path.push_back('M');
		explore(res, i_start, j_start+1, k_start, i_goal, j_goal, k_goal, path, max_step - 1);
		path.pop_back();
	}
	// turn left
	path.push_back('L');
	explore(res, i_start, j_start, k_start == 0 ? 3 : (k_start - 1), i_goal, j_goal, k_goal, path, max_step - 1);
	path.pop_back();
	// turn right
	path.push_back('R');
	explore(res, i_start, j_start, k_start == 3 ? 0 : (k_start + 1), i_goal, j_goal, k_goal, path, max_step - 1);
	path.pop_back();
}


vector<vector<char>> findAllPaths(int i_start, int j_start, char ks, int i_goal, int j_goal, char kg, int max_step) {
	vector<vector<char>> res;
	// deal with invalid input
	if (i_start < 1 || j_start>8 || i_goal < 1 || j_goal>8) {
		cerr << "Please check your positions..." << endl;
		return res;
	}
	if ((ks != 'E' && ks != 'S'&&ks != 'W'&&ks != 'N') || (kg != 'E' && kg != 'S'&& kg != 'W'&&kg != 'N')) {
		cerr << "Please check your directions..." << endl;
		return res;
	}
	// specify the direction input, E->S->W->N, +1->right, -1->left
	vector<char> dir{ 'E','S','W','N' };
	// get the corresponding position in the direction array to feed in explore function
	int k_start = 0, k_goal = 0;
	for (int i = 0; i < 4; ++i) {
		if (ks == dir[i]) {
			k_start = i;
		}
		if (kg == dir[i]) {
			k_goal = i;
		}
	}
	// use explore function to recursively find all the paths
	vector<char> path;
	explore(res, i_start, j_start, k_start, i_goal, j_goal, k_goal, path, max_step);
	return res;

}


/*
	Main function for testing the code
*/
int main()
{	
	int i_start = 2, j_start = 3, i_goal = 3, j_goal = 4, max_step = 4;
	char k_start = 'N', k_goal = 'S';
	vector<vector<char>> res;
	res = findAllPaths(i_start, j_start, k_start, i_goal, j_goal, k_goal, max_step);
	for (int i = 0; i < res.size(); ++i) {
		for (int j = 0; j < res[i].size(); ++j) {
			cout << res[i][j] << " ";
		}
		cout << endl;
	}
	system("pause");
    return 0;
}
